import("System")
import("System.Diagnostics")
import("String")

function initialize (version)
	return true, "NowWatchingプラグイン", 100, "mumei_himazin";
end

function OnRegistTweetBoxMenu ( hook )
	hook.text = "Now Watching";
	hook.tooltipText = "TVTestの見ている番組をシェアします。";
	hook.callbackFunction = "ClickedTweetBoxMenu";
end

function ClickedTweetBoxMenu  ( tweetBoxValue )
	local proc = getProcess ();
	if ( proc ~= nil ) then
		local title = split(proc,"-");
		tweetBoxValue.text = "Now Watching:"..title[2] .. " #nowwatching";
	end
end

function split(str, d)
    if string.find(str, d) == nil then
        return { str }
    end
    local result = {}
    local pat = "(.-)" .. d .. "()"
    local lastPos
    for part, pos in string.gmatch(str, pat) do
        table.insert(result, part)
        lastPos = pos
    end
    table.insert(result, string.sub(str, lastPos))
    return result
end

function getProcess ()
	local proc = Process.GetProcessesByName ( "TVTest" );
	if ( proc ~= nil ) then
		return proc[0].MainWindowTitle;
	else
		return nil;
	end
end

